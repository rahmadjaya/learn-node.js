$.getJSON('/json/data.json', function(json) {
	var data= json;
	$.each(data, function(i,item){
		var view = 
			'<div class="tutorial">'+
				'<div class="logo"><i>express</i></div>'+
				'<p>TUTORIAL</p>'+
				'<div class="title">'+item.title+'</div>'+
				'<p>By : name</p>'+
			'</div>'
		$('.content-2').append(view);
	})
})

$('#submitUser').click(function(e){
	e.preventDefault();
	datas = $('#formUser').serialize();
	console.log(datas);
	$.ajax({
		type : "POST",
		url  : "http://localhost:3000/v1/user",
		data : datas,
		success : function(data){
			console.log('berhasil');
			window.location.href="/try";
		},
		error : function(err){
			console.log('error');
		}
	})
})

$.getJSON('http://localhost:3000/v1/user', function(json){
	var data = json.result;
	$.each(data, function(i,item){
		var table =
			'<tr>'+
				'<td>'+(i+1)+'</td>'+
				'<td>'+item.username+'</td>'+
				'<td>'+item.email+'</td>'+
				'<td>'+item.phone+'</td>'+
				'<td><button onclick=edit('+"'"+item._id+"'"+')>Edit</button><button onclick=del('+"'"+item._id+"'"+')>Delete</button></td>'+
			'</tr>'
	$('#showData').append(table);	
	})
})
var x;
function edit(id){
	$('#formUser').hide();
	$('#table').hide();
	$('#formUpdate').show();
	console.log(id);
	$.getJSON('http://localhost:3000/v1/user/'+id, function(json){
		var data = json.result;
		$('#UserUpdate').val(data.username);
		$('#passUpdate').val(data.password);
		$('#emailUpdate').val(data.email);
		$('#phoneUpdate').val(data.phone);
		console.log(data);
		x=id;
	})
}

function del(id){
	$.ajax({
		type : "DELETE",
		url  : "http://localhost:3000/v1/user/"+id,
		success : function(data){
			console.log('berhasil');
			window.location.href="/try";
		},
		error : function(err){
			console.log('error');
		}
	})
}

$('#submitUpdate').click(function(e){
	e.preventDefault();
	datas = $('#formUpdate').serialize();
	console.log(datas);
	$.ajax({
		type : "PUT",
		url  : "http://localhost:3000/v1/user/"+x,
		data : datas,
		success : function(data){
			console.log('berhasil');
			window.location.href="/try";
		},
		error : function(err){
			console.log('error');
		}
	})
})


$('#formUser').hide();
$('#formUpdate').hide();
$('#Add').click(function(){
	$('#formUser').show();
	$('#table').hide();
	$('#formUpdate').hide();
})
$('#view').click(function(){
	$('#formUser').hide();
	$('#table').show();
	$('#formUpdate').hide();
})

$(document).ready(function () { 
	var options = { 
		beforeSubmit: showRequest,  // pre-submit callback 
		success: showResponse  // post-submit callback 
	}; 

// bind to the form's submit event 
	$('#frmUploader').submit(function () { 
		$(this).ajaxSubmit(options); 
		// always return false to prevent standard browser submit and page navigation 
		return false; 
	}); 
}); 

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
	alert('Uploading is starting.'); 
	return true; 
} 

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) { 
	alert('status: ' + statusText + '\n\nresponseText: \n' + responseText ); 
} 