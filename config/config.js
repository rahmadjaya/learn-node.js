var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'node-js-tutorial'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/node-js-tutorial-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'node-js-tutorial'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/node-js-tutorial-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'node-js-tutorial'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/node-js-tutorial-production'
  }
};

module.exports = config[env];
