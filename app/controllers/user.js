const saltRound = 10;
var multer = require('multer'); 
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var ls = require('local-storage');
var express = require('express');
	app = express();
	router = express.Router();
	mongoose = require('mongoose');
	user = mongoose.model('User');

module.exports = function(app) {
	app.use('/v1', router);
};

var Storage = multer.diskStorage({ 
    destination: function (req, file, callback) { 
        callback(null, "public/img"); 
    }, 
    filename: function (req, file, callback) { 
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname); 
    } 
}); 
var upload = multer({ storage: Storage }).array("imgUploader", 3);

router.post("/api/Upload", function (req, res) { 
    upload(req, res, function (err) { 
        if (err) { 
            return res.end("Something went wrong!"); 
        }  
        res.redirect('/paging');
    }); 
}); 

router.post('/user', function(req,res,next){
	var data = user.findOne({username:req.body.username});
	data.exec(function(err, result){
		if (result==null) {
			bcrypt.hash(req.body.password, saltRound, function(err, hash){
				var datauser = new user ({
					username : req.body.username,
					password : hash,
					email : req.body.email,
					phone : req.body.phone
				})
				datauser.save(function(err){
					if (err) {
						console.log(err)
					}
				});
				console.log('berhasil di tambah');
				res.json({message:"berhasil"});
			});
		} else {
			res.json({message: "username sudah terpakai"})
		}
	});
})

app.set('superSecret', ' ');
router.post('/login', function(req,res,next){
	var data = user.findOne({username:req.body.username});
	data.exec(function(err,result){
		if (result==null) {
			res.status(401).json({message:"Username tidak terdaftar"});
		} else {
			bcrypt.compare(req.body.password, result.password, function(err, compare){
				if (compare==true) {
					var token = jwt.sign(result, app.get('superSecret'),{
										 expiresIn : 60*60
					});
					ls.set('token', token);
					res.json({message: "Login berhasil",
						token:token}).status(200);
				} else {
					res.status(409).json({message:"Password salah"});
				}
			})
		}
	})
})

router.post('/logout', function(req,res,next){
	var decoded = jwt.decode(ls.get('token'));
	if (decoded==null) {
		res.json({message: "token tidak ditemukan"}).status(404);
	} else {
		var id_username = decoded.$__.id;
		ls.clear(id_username);
		res.json({message:"logout berhasil"}).status(404);
	};
})

router.get('/checktoken/:token', function(req, res, next){
  var decoded = jwt.decode(req.params.token);
  result = {
    uid:decoded._doc._id,
    username:decoded._doc.username
  }
  res.json({decoded});
});

router.get('/user' ,function(req,res,next){
	var data = user.find();
	data.exec(function(err,result){
		if (err)  return handleError(err);
		res.json({result}).status(200);
	})
})

router.get('/user/:id', function(req,res,next){
	var data = user.findOne({"_id":req.params.id});
	data.exec(function(err,result){
		if (err) return handleError(err);
		res.json({result}).status(200);
	})
})

router.put('/user/:id', function(req,res,next){
	var data = user.findOne({"_id":req.params.id});
	data.exec(function(err,result){
		if (result==null) {
			res.status(404).json({message : "not found"});
		} else {
			var dataold = {'_id': req.params.id};
			var datanew = {$set : req.body};
			var options = {};
			user.update(dataold, datanew, options, callback)
			function callback(err,numAffected){
				res.json({message: "update sukses"}).status(200);
			}
		}
	})
})

router.delete('/user/:id', function(req,res,next){
	var data = user.findOne({'_id':req.params.id});
	data.exec(function(err, result){
		if (result==null) {
			res.status(404).json({message : 'Not found'});
		} else {
			var data = user.remove({'_id':req.params.id});
			data.exec(function(err, result){
				if (err) {
					console.log(err);
				} else {
					res.json({message : "data terhapus"}).status(404);
				}
			})
		}
	})
})

