var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  Article.find(function (err, articles) {
    if (err) return next(err);
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
  });
});


router.get('/intro', function(req,res,next){
  res.render('intro', {
    title: "INTRO"
  })
})
router.get('/try', function(req,res,next){
  res.render('try', {
    title: "TRY"
  })
})

router.get('/template', function(req,res,next){
  res.render('template',{
    title : "TEMPLATE"
  })
})
router.get('/form', function(req,res,next){
  res.render('form', {
    title : "LOGIN"
  })
})
router.get('/crud', function(req,res,next){
  res.render('crud',{
    title: "CRUD"
  })
})
router.get('/paging', function(req,res,next){
  res.render('paging',{
    title: "PAGING"
  })
})