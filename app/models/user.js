var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
	mongoose.Promise = global.Promise;

var UserSchema = new Schema({
	username : String,
	password : String,
	email	 : String,
	phone	 : Number
});

mongoose.model('User', UserSchema);